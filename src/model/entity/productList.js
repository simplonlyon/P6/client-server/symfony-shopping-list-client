"use strict";

export class ProductList {
    constructor(id, name, products = []){
        this.id = id;
        this.name = name;
        this.products = products;
    }
}