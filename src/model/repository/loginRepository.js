"use strict";
import $ from 'jquery';

export class LoginRepository {
    constructor() {
        this.baseUrl = "http://localhost:8080/login_check";
    }
    /**
     * Méthode qui va chercher le token dans le sessionStorage, renvoie
     * null si jamais le token n'est pas stocké actuellement sur le pc 
     * (donc si on est pas connecté)
     */
    getToken() {
        return sessionStorage.getItem('token');
    }
    /**
     * 
     * La méthode login attend un username et un password et va
     * faire une requête ajax vers la route login de l'API.
     * Si les infos sont bonnes, on récupère le token dans le then
     * et on le stock en sessionStorage
     */
    login(username, password) {
        return $.ajax(this.baseUrl, {
            method: 'POST',
            data: JSON.stringify({username: username, password:password}),
            contentType: 'application/json'
        }).then(resp => {
            sessionStorage.setItem('token', resp.token);
        });
    }
    /**
     * La méthode logout retire le token du sessionStorage
     */
    logout() {
        sessionStorage.removeItem('token');
    }

}