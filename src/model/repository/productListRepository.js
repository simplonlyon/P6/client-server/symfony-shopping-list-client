"use strict";
import $ from 'jquery';
import { ProductList } from "../entity/productList";
import { LoginRepository } from './loginRepository';

export class ProductListRepository {
    constructor() {
        this.baseUrl = "http://localhost:8080/shopping-list";
        this.login = new LoginRepository();
    }

    createHeaders() {
        return {
            "Authorization": "Bearer "+this.login.getToken()
        }
    }

    findAll() {
        let url = this.baseUrl;
        let promise = $.ajax(url, {
            headers: this.createHeaders()
        }).then(function(response) {
            let productLists = [];
            response = JSON.parse(response);

            for (const element of response) {
                let instance = new ProductList(element.id, element.name, element.products);
                productLists.push(instance);
            }

            return productLists;
        });
        
        this.handleCatch(promise)

        return promise;
    }

    delete(id){
        let url = `${this.baseUrl}/${id}`;
        let promise = $.ajax(url, {method: "DELETE",headers: this.createHeaders()});
        
        
        this.handleCatch(promise)

        return promise;
    }

    add(productList){
        let url = this.baseUrl;
        let json = JSON.stringify(productList);
        let promise = $.ajax(url, {method: "POST", data: json, headers: this.createHeaders()});
        
        this.handleCatch(promise)

        return promise;
    }
    /**
     * On gère tous les catch du repository au même endroit et on
     * lui dit de faire que si le status http est 401 (unauthorized)
     * on fait un logout pour supprimer le token actuellement stocké.
     * Ca permettra de gérer l'expiration des tokens
     */
    handleCatch(promise) {

        promise.catch(error => {
            if(error.status === 401) {
                this.login.logout();   
            }
            throw error;
        });
    }
}