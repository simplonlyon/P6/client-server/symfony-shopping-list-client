"use strict";

import { ProductListRepository } from "../model/repository/productListRepository";
import { HomeView } from "../view/home";
import $ from "jquery";
import { LoginRepository } from "../model/repository/loginRepository";

export class HomeController {
    constructor(){
        this.logRepo = new LoginRepository();
        let repo = new ProductListRepository();

        /**
         * l'event pour le login va faire appelle à la méthode login
         * du LoginRepository, et si ça marche, on fait une requête
         * sur le ProductListRepository et on instancie une HomeView
         */
        $(document).on('login', (event, credentials) => {
            this.logRepo.login(credentials.username, credentials.password)
            .then( () => {
                
                repo.findAll().then(resp => new HomeView(resp))
            });
        });

        $(document).on("logout", () => {
            this.logRepo.logout();
            this.checkToken();
        });

        let promise = repo.findAll();
        
        promise.then(response => {
            new HomeView(response);
        }).catch( () => this.checkToken());;

        $(document).on("deleteProductList", (event, productList) => {
            repo.delete(productList.id).then(() => {
                repo.findAll().then(response => {
                    new HomeView(response);
                }).catch( () => this.checkToken());
            });
        });

        $(document).on("addProductList", (event, productList) => {
            console.log(productList);
            repo.add(productList).then(() => {
                repo.findAll().then(response => {
                    new HomeView(response);
                });
            }).catch( () => this.checkToken());;
        });


    }

    checkToken() {
        /**
         * Si jamais il n'y a pas de token actuellement stocké,
         * on instancie le HomeView en lui disant d'afficher le login
         */
        if(!this.logRepo.getToken()) {
            new HomeView([], true);
            return;
        }
    }
}