"use strict";
import $ from "jquery";
import { ProductList } from "../model/entity/productList";

export class HomeView {
    constructor(list = [], showLog = false) {
        $("body").empty();
        if (!showLog) {
            let ul = document.createElement("ul");
            let form = document.createElement("form");
            let input = document.createElement("input");
            let submit = document.createElement("input");
            let logoutBtn = document.createElement("button");

            submit.setAttribute("type", "submit");
            submit.setAttribute("value", "Ok");
            $(form).append(input, submit);

            $(form).submit(event => {
                event.preventDefault();
                let productList = new ProductList(null, input.value);
                $(document).trigger("addProductList", productList);
            });

            list.forEach(productList => {
                let li = document.createElement("li");
                let span = document.createElement("span");
                let button = document.createElement("button");

                button.textContent = "supprimer";
                $(button).click(() => {
                    $(document).trigger("deleteProductList", productList);
                });

                span.textContent = productList.name;

                $(li).append(span, button);
                $(ul).append(li);
            });

            $(logoutBtn).text("Logout")
            .on('click', () => {
                $(document).trigger("logout");
            });

            $("body").append(ul)
            .append(form)
            .append(logoutBtn);
        } else {
            /**
             * Si jamais on indique à la vue qu'elle doit afficher
             * le login, on créer un formulaire de login qui déclenchera
             * un event 'login' sur le document en passant les credentials
             * en paramètre d'event
             */
            let form = document.createElement('form');
            let inputUsername = document.createElement('input');
            let inputPassword = document.createElement('input');
            let btnSubmit = document.createElement('button');

            $(inputUsername).attr('placeholder', 'Username');
            $(inputPassword).attr('placeholder', 'Password');
            $(btnSubmit).text('Login');
            
            $(form).append(inputUsername)
            .append(inputPassword)
            .append(btnSubmit)
            .on('submit', event => {
                event.preventDefault();
                $(document).trigger('login', {
                    username: $(inputUsername).val(),
                    password: $(inputPassword).val()
                });
            });

            $('body').append(form);
        }
    }
}